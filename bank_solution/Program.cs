﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank2309
{
    class Program
    {
        public static void Main(string[] args)
        {
            Bank poalim = new Bank("Hapoalim", "Hertzel 30 Rishon lezion");

            Customer roy = new Customer(1, "roi", "03-555555");
            Account account_roi = new Account(roy, 30000);
            Account account_roi_bus = new Account(roy, 50000);
            //Account account_roi_joined = account_roi + account_roi_bus;
            //int roi_would_have_this_balance_after_add_if_occur = account_roi + 500;
            //bool it_is_possible_but_dont_do_it = account_roi - 1_000_000;
            //if (account_roi == account_roi_bus)
            //{

            //}
            poalim.AddNewCustomer(roy);
            poalim.OpenNewAccount(roy, account_roi);
            poalim.OpenNewAccount(roy, account_roi_bus);
            //poalim.OpenNewAccount(roy, account_roi_bus);

            poalim.Despoit(account_roi, 8000);

            int _8001 = account_roi + 1;

            poalim.Despoit(account_roi_bus, 18000);
            int total = poalim.GetCustomerTotalBalance(roy);
            Console.WriteLine($"Total = {total}");

            Account joint = poalim.JoinAccounts(account_roi, account_roi_bus);
            total = poalim.GetCustomerTotalBalance(roy);
            Console.WriteLine($"Total = {total}");
            poalim.Despoit(account_roi_bus, 18000);

            Console.WriteLine("\n\n\nGoodbye...");

        }
    }
}
