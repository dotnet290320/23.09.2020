﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Bank2309
{
    public class Account
    {
        private static int _numberOfAcc;
        private readonly int _accountNumber;
        private readonly Customer _accountOwner;
        private int _maxMinusAllowed;

        public Account(Customer accountOwner, int monthlyIncome)
        {
            _maxMinusAllowed = -(monthlyIncome * 3);
            _accountOwner = accountOwner;
            _accountNumber = ++_numberOfAcc;
        }

        public int AccountNumber { get { return _accountNumber; } }
        public int Balance { get; private set; }
        public Customer AccountOwner { get { return _accountOwner; } }
        public int MaxMinusAllowed { get { return _maxMinusAllowed; } }
        public override bool Equals(object obj)
        {
            return this == obj as Account;
        }
        public void Add (int amount)
        {
            Balance = Balance + amount;
        }
        public void Subtract(int amount, bool bank_profit_operation = false)
        {
            if (Balance - amount < _maxMinusAllowed && !bank_profit_operation)
                throw new BalanceException($"you cant redraw {amount} because it overstep the maximum minus " +
                    $"allowed which is {_maxMinusAllowed} from {this.ToString()}");
            else
            Balance -= amount;
        }

        public override int GetHashCode()
        {
            return _accountNumber;
        }

        public override string ToString()
        {
            return $"account number: {AccountNumber}, account owner: {AccountOwner}, balance: {Balance}, maximum minimum allowed: {MaxMinusAllowed}";
        }

        public static bool operator ==(Account a1, Account a2)
        {
            if (a1 is null && a2 is null)
                return true;
            if (a1 is null || a2 is null)
                return false;
            return a1._accountNumber == a2._accountNumber;
        }
        public static bool operator !=(Account a1, Account a2)
        {
            if (a1 is null && a2 is null)
                return false;
            if (a1 is null || a2 is null)
                return true;
            return a1._accountNumber != a2._accountNumber;
        }
        public static Account operator +(Account a1, Account a2)
        {
            // assert
            // same owner?
            if (a1.AccountOwner != a2.AccountOwner)
            {
                throw new NotSameCustomerException($"failed operator+ since not same customer owner. " +
                    $"account owner 1 = {a1}, account owner 2 = {a2}");
            }
            if (a1.AccountNumber == a2.AccountNumber)
            {
                throw new AccountAlreadyExistException($"failed operator+ since both account are the same {a1}");
            }

            // join accounts
            int higher = Math.Max(Math.Abs(a1.MaxMinusAllowed), Math.Abs(a2.MaxMinusAllowed));
            Account new_account = new Account(a1.AccountOwner, higher / 3);
            new_account.Balance = a1.Balance + a2.Balance;

            return new_account;
        }
        public static int operator +(Account a, int amount)
        {
            return a.Balance + amount;
        }
        public static bool operator -(Account a, int amount)
        {
            if (a.Balance - amount < a.MaxMinusAllowed)
                return false;

            return true;
        }
    }
}
