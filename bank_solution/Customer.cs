﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank2309
{
    public class Customer
    {
        private static int _numberOfCust = 0;
        private readonly int _customerID;
        private readonly int _CustomerNumber;

        public string Name { get; private set; }
        public string PhNumber { get; private set; }
        public int CustomerId { get { return _customerID; } }
        public int CustomerNumber { get { return _customerID; } }

        public Customer(int customerID, string name, string phNumber)
        {
            _customerID = customerID;
            Name = name;
            PhNumber = phNumber;
            _CustomerNumber = ++_numberOfCust;
        }

        public override bool Equals(object obj)
        {
            return this == obj as Customer;
        }

        public override int GetHashCode()
        {
            return CustomerId;
        }

        public static bool operator == (Customer c1, Customer c2)
        {
            if (c1 is null && c2 is null)
                return true;
            if (c1 is null || c2 is null)
                return false;
            return c1.CustomerNumber == c2.CustomerNumber;
        }
        public static bool operator !=(Customer c1, Customer c2)
        {
            if (c1 is null && c2 is null)
                return false;
            if (c1 is null || c2 is null)
                return true;
            return c1.CustomerNumber != c2.CustomerNumber;
        }
        public override string ToString()
        {
            return $"name: {Name}, phone number: {PhNumber}, id: {CustomerId}, customer number: {CustomerNumber}";
        }
    }
}
