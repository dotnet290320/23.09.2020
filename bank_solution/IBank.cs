﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank2309
{
    public interface IBank
    {
        string Name { get; }
        string Adress { get; }
        int CustomerCount { get; }
    }
}
