﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank2309
{
    public class Bank : IBank
    {
        private List<Account> _accounts;
        private List<Customer> _customers;
        private Dictionary<int, Customer> m_map_customerid_to_customer;
        private Dictionary<int, Customer> m_map_customernumber_to_customer;
        private Dictionary<int, Account> m_map_accountnumber_to_account;
        private Dictionary<Customer, List<Account>> m_map_customer_obj_to_list_accounts;
        private long _total_money_in_bank;
        private double _proits;
        private string _addess;
        private string _name;

        public string Name { get { return _name; } }
        public string Adress { get { return _addess; } }
        public int CustomerCount { get { return _customers.Count; } }

        public Bank(string name, string address)
        {
            _name = name;
            _addess = address;

            _accounts = new List<Account>();
            _customers = new List<Customer>();
            m_map_customerid_to_customer = new Dictionary<int, Customer>();
            m_map_customernumber_to_customer = new Dictionary<int, Customer>();
            m_map_accountnumber_to_account = new Dictionary<int, Account>();
            m_map_customer_obj_to_list_accounts = new Dictionary<Customer, List<Account>>();
        }

        internal Customer GetCustomerById(int customer_id)
        {
            m_map_customerid_to_customer.TryGetValue(customer_id, out Customer result);
            return result;
        }

        internal Customer GetCustomerByNumber(int customer_number)
        {
            m_map_customernumber_to_customer.TryGetValue(customer_number, out Customer result);
            return result;
        }

        internal List<Account> GetAccountsByCustomer(Customer customer)
        {
            m_map_customer_obj_to_list_accounts.TryGetValue(customer, out List<Account> result);
            return result;
        }

        internal void AddNewCustomer(Customer customer)
        {
            if (_customers.Contains(customer))
                throw new CustomerAlreadyExistException($"{customer} already exist in Bank {this} {DateTime.Now}");

            Customer result = _customers.FirstOrDefault(c => c.CustomerId == customer.CustomerId);
            // no without linq ---
            //Customer result = null;
            //foreach (Customer item in _customers)
            //{
            //    if (item.CustomerId == customer.CustomerId)
            //    {
            //        result = item;
            //        break;
            //    }
            //}
            // --- result = customer or null
            if (result != null)
            {
                throw new CustomerAlreadyExistException($"customer {customer} with same id already exist in our list. here: {result}");
            }

            Customer bad_result = _customers.FirstOrDefault(c => c.CustomerNumber == customer.CustomerNumber);
            if (result != null)
            {
                throw new CustomerAlreadyExistException($"customer {customer} with same customer number  already exist in our list. here: {result}");
            }
            if (m_map_customerid_to_customer.ContainsKey(customer.CustomerId))
            {
                throw new CustomerAlreadyExistException($"customer {customer} exist in m_map_customerid_to_customer. system unexpected error. please contact support");
            }
            if (m_map_customernumber_to_customer.ContainsKey(customer.CustomerNumber))
            {
                throw new CustomerAlreadyExistException($"customer {customer} exist in m_map_customernumber_to_customer. system unexpected error. please contact support");
            }
            if (m_map_customer_obj_to_list_accounts.ContainsKey(customer))
            {
                throw new CustomerAlreadyExistException($"customer {customer} exist in m_map_customer_obj_to_list_accounts. system unexpected error. please contact support");
            }

            // all good now we can add
            _customers.Add(customer);
            m_map_customerid_to_customer.Add(customer.CustomerId, customer);
            m_map_customernumber_to_customer.Add(customer.CustomerNumber, customer);
            m_map_customer_obj_to_list_accounts.Add(customer, new List<Account>());
        }

        public void OpenNewAccount(Customer customer, Account account)
        {
            // assert
            if (!_customers.Contains(customer))
                throw new CustomerNotFoundException("....");
            if (_accounts.Contains(account))
                throw new AccountAlreadyExistException("...");
            if (m_map_accountnumber_to_account.ContainsKey(account.AccountNumber))
                throw new AccountAlreadyExistException("...");

            // assert also maps ... like in method AddNewCustomer
            // .....
            List<Account> _list_of_accounts = m_map_customer_obj_to_list_accounts[customer];
            if (_list_of_accounts.Contains(account))
                throw new AccountAlreadyExistException("...");
            // bad: (do NOT write this)
            // m_map_customer_obj_to_list_accounts[customer] = _list_of_accounts;

            // example:
            //Dictionary<int, int> map_int_to_int = new Dictionary<int, int>();
            //map_int_to_int.Add(1, 2);
            //int number = map_int_to_int[1];
            //number++; // will not affect dictionary

            _list_of_accounts.Add(account); // this will affect dictionary directly!
            _accounts.Add(account);
            m_map_accountnumber_to_account.Add(account.AccountNumber, account);
        }

        public int Despoit(Account account, int amount)
        {
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException("...");

            account.Add(amount);
            _total_money_in_bank += amount;
            return account.Balance;
        }

        public int Withdraw(Account account, int amount)
        {
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException("...");
            
            account.Subtract(amount);
            _total_money_in_bank -= amount;

            return account.Balance;
        }

        public int GetCustomerTotalBalance(Customer customer)
        {
            if (!_customers.Contains(customer))
                throw new AccountNotFoundException("...");

            List<Account> accounts_for_this_customer = m_map_customer_obj_to_list_accounts[customer]; // if not exist then exception


            int sum = 0;
            accounts_for_this_customer.ForEach(a => sum = sum + a.Balance);
            //foreach (Account acc in accounts_for_this_customer)
            //{
            //    sum = sum + acc.Balance;
            //}
            int sum_quicker = accounts_for_this_customer.Sum(a => a.Balance);
            int sum_another_way = accounts_for_this_customer.Select(a => a.Balance).Sum(); // {1,3,5,9}

            return sum;
        }

        public void CloseAccount(Account account, Customer customer)
        {
            // assert
            if (!_accounts.Contains(account))
                throw new AccountNotFoundException("..");
            if (!_customers.Contains(customer))
                throw new CustomerNotFoundException("...");
            if (account.AccountOwner != customer)
                throw new NotSameCustomerException("... customer who tries to close this account is not the owner ...");

            m_map_accountnumber_to_account.Remove(account.AccountNumber);
            List<Account> accounts_for_this_customer = m_map_customer_obj_to_list_accounts[customer]; // if not exist then exception

            if (!accounts_for_this_customer.Contains(account))
                throw new AccountNotFoundException($"... system unexpected error. please contact support"); // maybe exception not required ?

            accounts_for_this_customer.Remove(account);
            // bad: (do NOT write this)
            // m_map_customer_obj_to_list_accounts[customer] = _list_of_accounts;

            _accounts.Remove(account);
        }

        public void ChargeAnnual(float percentage)
        {
            _proits = 0;
            _accounts.ForEach(acc =>
            {
                double amla = Math.Abs(acc.Balance * percentage);
                if (acc.Balance < 0)
                    amla *= 2;

                _proits += amla;

                acc.Subtract((int)amla, true); 
            });
        }

        public Account JoinAccounts(Account acc1, Account acc2)
        {
            // assert
            if (!_accounts.Contains(acc1))
                throw new AccountNotFoundException("..");
            if (!_accounts.Contains(acc2))
                throw new AccountNotFoundException("..");
            if (acc1.AccountOwner != acc2.AccountOwner)
                throw new NotSameCustomerException("... cannot join 2 accounts with different owner ...");
            // this is redudnent since we alreay check it in the operator+ ...
            if (acc1 == acc2)
                throw new SameAccountException("... cannot join account to himself ... ");

            Customer owner = acc1.AccountOwner;
            Account joint = acc1 + acc2;
            OpenNewAccount(owner, joint);
            
            // remove acc1 and acc2 from bank
            CloseAccount(acc1, owner);
            CloseAccount(acc2, owner);

            return joint;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Name} {Adress}";
        }
    }
}
