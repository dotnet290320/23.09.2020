﻿using System;
using System.Runtime.Serialization;

namespace Bank2309
{
    [Serializable]
    internal class SameAccountException : Exception
    {
        public SameAccountException()
        {
        }

        public SameAccountException(string message) : base(message)
        {
        }

        public SameAccountException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SameAccountException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}